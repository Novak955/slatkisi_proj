<?php 
if(!isset($config)) { $config = []; }
$config['website'] = [
    'url' => 'http://localhost/slatkisi_proj/',
    'path' => '/slatkisi_proj/',
    'uploads_dir' => realpath(__DIR__ . '/../../') . '/uploads/',
    'uploads_url' => '/slatkisi_proj/uploads/',
    'site_name' => 'Slatkiši',
    'site_email' => 'test@test.com',
    'session_prefix' => 'myapp_'
];