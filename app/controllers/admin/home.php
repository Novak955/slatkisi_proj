<?php 
Class HomeAdminController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
        $data = [];

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];

        $view = $this->app['view'];
        $view->render('admin/header', $data);
        $view->render('admin/menu', $data);
        $view->render('admin/home', $data);
        $view->render('admin/footer', $data);
    }
}