<?php 
Class LoginAdminController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
        $data = [];

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];
        
        $username = '';
        $error = false;

        if(isset($_POST['username']) && isset($_POST['password']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $password_hash = hash('sha512', $password);

            if($this->app['user']->getByCredentials($username, $password_hash)) {
                $this->app['login']->login($username);
                Flight::redirect($this->app['config']['website']['url'] . 'admin');
                exit;
            } else {
                $error = 'Pogrešno korisničko ime i/ili lozinka!';
            }
        }

        $data['username'] = $username;
        $data['error'] = $error;

        $view = $this->app['view'];
        $view->render('admin/header', $data);
        $view->render('admin/login', $data);
        $view->render('admin/footer', $data);
    }
}