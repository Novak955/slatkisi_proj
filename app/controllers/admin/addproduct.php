<?php 
Class AddProductAdminController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
        $data = [];

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];

        $data['categories'] = $this->app['models']['category']->list();

        $title = '';
        $short_description = '';
        $long_description = '';
        $unit_of_measure = '';
        $price_per_unit = '';
        $category_id = '';
        $error = false;

        if(isset($_POST['title']) && isset($_POST['picture_id']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = $_POST['title'];
            $short_description = $_POST['short_description'];
            $long_description = $_POST['long_description'];
            $unit_of_measure = $_POST['unit_of_measure'];
            $price_per_unit = $_POST['price_per_unit'];
            $category_id = $_POST['category_id'];
            $picture_id = $_POST['picture_id'];

            $view = $this->app['view'];
            
            if(!$picture_id) {
                $error = 'Slika nije dodata! Molimo pokušajte ponovo. ';
            } else {
                if(!$this->app['models']['product']->getByTitle($title)) {
                    $this->app['models']['product']->add($title, $short_description, $long_description, $picture_id, $unit_of_measure, $price_per_unit, $category_id);
                    
                    $redirect_uri = $data['site_url'] . 'admin/products';

                    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                        $view->json([
                            'success' => (bool) !$error,
                            'error' => $error,
                            'redirect' => (!$error ? $redirect_uri : '')
                        ], true);
                    } else {
                        Flight::redirect($redirect_uri);
                    }
                } else {
                    $error = 'Uneti naslov proizvoda već postoji!';
                }
            }
        }

        $data['title'] = $title;
        $data['short_description'] = $short_description;
        $data['long_description'] = $long_description;
        $data['unit_of_measure'] = $unit_of_measure;
        $data['price_per_unit'] = $price_per_unit;
        $data['category_id'] = $category_id;
        // $picture_id
        $data['error'] = $error;


        $view = $this->app['view'];
        $view->render('admin/header', $data);
        $view->render('admin/menu', $data);
        $view->render('admin/addproduct', $data);
        $view->render('admin/footer', $data);
    }
}