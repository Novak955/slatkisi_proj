<?php 
Class EditProductAdminController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
        $product_id = $args['id'];

        if(!$product_id) { throw new Exception('Niste prosledili ID proizvoda!'); }

        $data = [];

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];

        $data['categories'] = $this->app['models']['category']->list();

        $product = $this->app['models']['product']->getById($product_id);
        
        $title = '';
        $short_description = '';
        $long_description = '';
        $unit_of_measure = '';
        $price_per_unit = '';
        $category_id = '';
        $picture_thumb_url = '';
        $picture_id = '';

        if(isset($product['product_id'])) {
            $title = $product['product_title'];
            $short_description = $product['product_short_description'];
            $long_description = $product['product_long_description'];
            $unit_of_measure = $product['product_unit_of_measure'];
            $price_per_unit = $product['product_price_per_unit'];
            $category_id = $product['product_category_id'];
            $picture_thumb_url = $product['picture_thumb_url'];
            $picture_id = $product['picture_id'];
        } else {
            throw new Exception('Proizvod nije pronađen!');
        }

        $error = false;

        $view = $this->app['view'];

        if(isset($_POST['title']) && isset($_POST['picture_id']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = $_POST['title'];
            $short_description = $_POST['short_description'];
            $long_description = $_POST['long_description'];
            $unit_of_measure = $_POST['unit_of_measure'];
            $price_per_unit = $_POST['price_per_unit'];
            $category_id = $_POST['category_id'];
            $picture_id = $_POST['picture_id'];
            
            if(!$picture_id) {
                $error = 'Slika nije dodata! Molimo pokušajte ponovo. ';
            } else {
                $this->app['models']['product']->update($product_id, $title, $short_description, $long_description, $picture_id, $unit_of_measure, $price_per_unit, $category_id);

                $redirect_uri = $data['site_url'] . 'admin/products';

                if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                    $view->json([
                        'success' => (bool) !$error,
                        'error' => $error,
                        'redirect' => (!$error ? $redirect_uri : '')
                    ], true);
                } else {
                    Flight::redirect($redirect_uri);
                }
            }
        }

        $data['id'] = $product_id;
        $data['title'] = $title;
        $data['short_description'] = $short_description;
        $data['long_description'] = $long_description;
        $data['unit_of_measure'] = $unit_of_measure;
        $data['price_per_unit'] = $price_per_unit;
        $data['category_id'] = $category_id;
        $data['picture_thumb_url'] = $picture_thumb_url;
        $data['picture_id'] = $picture_id;
        $data['error'] = $error;


        $view->render('admin/header', $data);
        $view->render('admin/menu', $data);
        $view->render('admin/editproduct', $data);
        $view->render('admin/footer', $data);
    }
}