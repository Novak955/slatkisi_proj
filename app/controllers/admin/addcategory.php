<?php 
Class AddCategoryAdminController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
        $data = [];

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];

        $title = '';
        $error = false;

        if(isset($_POST['title']) && isset($_POST['picture_id']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = $_POST['title'];
            $picture_id = $_POST['picture_id'];
            
            if(!$picture_id) {
                $error = 'Slika nije dodata! Molimo pokušajte ponovo. ';
            } else {
                if(!$this->app['models']['category']->getByTitle($title)) {
                    $this->app['models']['category']->add($title, $picture_id);
                } else {
                    $error = 'Uneta kategorija već postoji!';
                }
            }
        }

        $view = $this->app['view'];

        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $view->json([
                'success' => (bool) !$error,
                'error' => $error,
                'redirect' => (!$error ? $data['site_url'] . 'admin/categories' : '')
            ], true);
        }

        $data['title'] = $title;
        $data['error'] = $error;


        $view->render('admin/header', $data);
        $view->render('admin/menu', $data);
        $view->render('admin/addcategory', $data);
        $view->render('admin/footer', $data);
    }
}