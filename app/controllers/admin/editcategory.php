<?php 
Class EditCategoryAdminController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function setVisibility($args = []) {
        $category_id = $args['id'];

        $visibility = $args['hide'] == '1' ? '1' : '0';

        if(!$category_id) { throw new Exception('Niste prosledili ID kategorije!'); }

        $this->app['models']['category']->setVisibility($category_id, $visibility);

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];
        
        $error = false;

        $redirect_uri = $data['site_url'] . 'admin/categories';

        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $view->json([
                'success' => (bool) !$error,
                'error' => $error,
                'redirect' => (!$error ? $redirect_uri : '')
            ], true);
        } else {
            Flight::redirect($redirect_uri);
        }
    }
    public function run($args = []) {
        $category_id = $args['id'];

        if(!$category_id) { throw new Exception('Niste prosledili ID kategorije!'); }

        $data = [];

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];

        $data['categories'] = $this->app['models']['category']->list();

        $category = $this->app['models']['category']->getById($category_id);
        
        $title = '';
        $short_description = '';
        $long_description = '';
        $unit_of_measure = '';
        $price_per_unit = '';
        $picture_thumb_url = '';
        $picture_id = '';

        if(isset($category['category_id'])) {
            $title = $category['category_title'];
            $picture_id = $category['picture_id'];
            $picture_thumb_url = $category['picture_thumb_url'];
        } else {
            throw new Exception('Kategorija nije pronađena!');
        }

        $error = false;

        $view = $this->app['view'];

        if(isset($_POST['title']) && isset($_POST['picture_id']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = $_POST['title'];
            $picture_id = $_POST['picture_id'];
            
            if(!$picture_id) {
                $error = 'Slika nije dodata! Molimo pokušajte ponovo. ';
            } else {
                $this->app['models']['category']->update($category_id, $title, $picture_id);

                $redirect_uri = $data['site_url'] . 'admin/categories';

                if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                    $view->json([
                        'success' => (bool) !$error,
                        'error' => $error,
                        'redirect' => (!$error ? $redirect_uri : '')
                    ], true);
                } else {
                    Flight::redirect($redirect_uri);
                }
            }
        }

        $data['id'] = $category_id;
        $data['title'] = $title;
        $data['picture_id'] = $picture_id;
        $data['picture_thumb_url'] = $picture_thumb_url;
        $data['error'] = $error;

        $view->render('admin/header', $data);
        $view->render('admin/menu', $data);
        $view->render('admin/editcategory', $data);
        $view->render('admin/footer', $data);
    }
}