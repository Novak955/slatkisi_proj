<?php 
Class PictureUploadAPIAdminController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
        $data = [];

        $error = false;

        $view = $this->app['view'];

        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if(isset($_FILES['file']['tmp_name'])) {
                $thumb_size = 320;
                $target_dir = $this->app['config']['website']['uploads_dir'];
                $uploads_url = $this->app['config']['website']['uploads_url'];
                $picture_filename = basename($_FILES['file']['name']);
                $target_file = $target_dir . $picture_filename;
                $picture_url = $uploads_url . $picture_filename;
                $picture_thumb_url = $uploads_url . 'thumb_' . $picture_filename;
                $target_file_thumb = $target_dir . 'thumb_' . $picture_filename;
                $image_file_type = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
                $check_is_image = getimagesize($_FILES['file']['tmp_name']);
                if(!$check_is_image) {
                    $view->json([
                        'success' => false,
                        'error' => 'Uploadovani fajl nije slika!'
                    ], true);
                } else {
                    if (file_exists($target_file)) {
                        $view->json([
                            'success' => false,
                            'error' => 'Fajl već postoji. Preimenujte fajl i pokušajte ponovo!'
                        ], true);
                    } else {
                        if ($_FILES['file']['size'] > 500000) {
                            $view->json([
                                'success' => false,
                                'error' => 'Fajl je veći od 500 KB! Kompresujte sliku i pokušajte ponovo. '
                            ], true);
                        } else {
                            if($image_file_type != "jpg" && $image_file_type != "jpeg") {
                                $view->json([
                                    'success' => false,
                                    'error' => 'Tip slike nije JPG! Dozvoljene su samo JPG slike! '
                                ], true);
                            } else {
                                $im = imagecreatefromjpeg($_FILES['file']['tmp_name']);
                                $mw = $mh = $thumb_size;
                                $nm = imagecreatetruecolor($mw, $mh);
                                $ow = imagesx($im);
                                $oh = imagesy($im);
                                $nw = $oh * $mw / $mh;
                                $nh = $ow * $mw / $mh;
                                if($nw > $ow){
                                    $yo = round(($oh - $nh) / 2);
                                    imagecopyresampled($nm, $im, 0, 0, 0, $yo, $mw, $mh, $ow, $nh);
                                } else {
                                    $xo = round(($ow - $nw) / 2);
                                    imagecopyresampled($nm, $im, 0, 0, $xo, 0, $mw, $mh, $nw, $nh);
                                }
                                imagejpeg($nm, $target_file_thumb, 90);
                                imagedestroy($im);
                                imagedestroy($nm);
                                move_uploaded_file($_FILES['file']['tmp_name'], $target_file);
                                $response_id = $this->app['models']['picture']->add($picture_filename, $picture_url, $picture_thumb_url);
                                $view->json([
                                    'success' => (bool) !$error,
                                    'error' => $error,
                                    'response_id' => $response_id
                                ], true);
                            }
                        }
                    }
                }
            } else {
                $view->json([
                    'success' => false,
                    'error' => 'Niste odabrali fajl za upload!'
                ], true);
            }
        } else {
            $view->json([
                'success' => false,
                'error' => 'Ova metoda je dostupna samo preko XHR!'
            ], true);
        }
    }
}