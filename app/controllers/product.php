<?php 
Class ProductController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
        $data = [];

        if(isset($args['id']) && $args['id']) {
            $id = $args['id'];
        } else {
            throw new Exception("Nije prosleđen ID proizvoda!");
        }

        $data['product'] = $this->app['models']['product']->getById($id);

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];

        $data['categories'] = $this->app['models']['category']->list();

        $view = $this->app['view'];
        $view->render('header', $data);
        $view->render('product', $data);
        $view->render('footer', $data);
    }
}