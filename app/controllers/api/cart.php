<?php 
Class CartAPIController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
        $data = [];
		
		if($_SERVER['REQUEST_METHOD'] != 'POST') {
			throw new Exception('Metoda mora biti POST!');
		}

        if(isset($_POST['action']) && $_POST['action']) {
            $action = $_POST['action'];
        } else {
            throw new Exception("Nije prosleđena željena radnja!");
        }
		
		$error = false;
		
		$cart = $this->app['models']['cart']->get();
		
		$data = [
			'success' => (bool) !$error
		];
		
		if($action == 'check') {
			$item = $_POST['product'];
			
			$data = [
				'success' => true,
				'result' => $this->app['models']['cart']->exists($item)
			];
		}
		
		if($action == 'add') {
			$id = $_POST['id'];
			$qty = $_POST['quantity'];
			
			$this->app['models']['cart']->add($id, $qty);
			
			$data = [
				'success' => true
			];
		}
		
		if($action == 'update') {
			$product = $_POST['product'];
			$qty = $_POST['quantity'];
			
			$this->app['models']['cart']->update($product, $qty);
			
			$data = [
				'success' => true
			];
		}
		
		if($action == 'remove') {
			$product = $_POST['product'];
			
			$this->app['models']['cart']->remove($product);
			
			$data = [
				'success' => true
			];
		}
		
		if($action == 'empty') {
			$this->app['models']['cart']->empty();
			
			$data = [
				'success' => true
			];
		}
		
		if($action == 'list') {
			$items = $this->app['models']['cart']->get();
			
			$data = [
				'success' => true,
				'items' => $items
			];
		}
		
		if($action == 'total') {
			$total = $this->app['models']['cart']->total();
			
			$data = [
				'success' => true,
				'total' => $total
			];
		}
		
        $view = $this->app['view'];
		$view->json($data, true);
    }
}