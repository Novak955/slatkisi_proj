<?php 
Class CheckoutController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
		
		$email_admin = $this->app['config']['website']['site_email'];
		
        $data = [];

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];

        $data['cart'] = $this->app['models']['cart']->get();
		
		foreach($data['cart'] as $i => $item) {
			$data['cart'][$i]['total'] = $item['quantity'] * $item['price'];
		}
		
        if(isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['address']) && isset($_POST['email']) && isset($_POST['note']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
			$cart = $data['cart'];
			$data['first_name'] = $_POST['first_name'];
			$first_name = $data['first_name'];
			$data['last_name'] = $_POST['last_name'];
			$last_name = $data['last_name'];
			$data['email'] = $_POST['email'];
			$email = $data['email'];
			$data['address'] = $_POST['address'];
			$address = $data['address'];
			$data['note'] = $_POST['note'];
			$note = $data['note'];
			$order_text = '';
			foreach($cart as $item) {
				$order_text .= $item['title'] . "\r\n";
				$order_text .= $item['quantity'] . ' x ' . $item['price'] . ' din = ' . $item['total'] . ' din' . "\r\n";
				$order_text .= "\r\n";
			}
			$email_string = '';
			$email_string .= '=========== NOVA PORUDZBINA NA SAJTU SLATKISI ===========' . "\r\n";
			$email_string .= 'Ime: ' . $first_name . "\r\n";
			$email_string .= 'Prezime: ' . $last_name . "\r\n";
			$email_string .= 'Email: ' . $email . "\r\n";
			$email_string .= 'Adresa: ' . $address . "\r\n";
			$email_string .= 'Napomena: ' . $note . "\r\n";
			$email_string .= 'Porudzbina: ' . "\r\n". $order_text . "\r\n";
			$email_string .= '==================== KRAJ PORUDZBINE ====================' . "\r\n";
			
			// Komentarisano jer Email ne radi na localhost-u
			//mail($email_admin, 'NOVA PORUDZBINA NA SAJTU SLATKISI', $email_string);
			//mail($email, 'NOVA PORUDZBINA NA SAJTU SLATKISI', $email_string);
			
			$data['order_info'] = $email_string;
			
			$view = $this->app['view'];
			$view->render('header', $data);
			$view->render('ordered', $data);
			$view->render('footer', $data);
			exit;
		}

        $view = $this->app['view'];
        $view->render('header', $data);
        $view->render('checkout', $data);
        $view->render('footer', $data);
    }
}