<?php 
Class HomeController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
        $data = [];

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];

        $data['categories'] = $this->app['models']['category']->list();

        $view = $this->app['view'];
        $view->render('header', $data);
        $view->render('home', $data);
        $view->render('footer', $data);
    }
}