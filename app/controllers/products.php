<?php 
Class ProductsController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    public function run($args = []) {
        $data = [];
        
        $category_id = '';

        if(isset($_GET['category'])) {
            $category_id = $_GET['category'];
        }
        
        $category = $this->app['models']['category']->getById($category_id);

        $data['site_name'] = $this->app['config']['website']['site_name'];
        $data['site_url'] = $this->app['config']['website']['url'];

        $data['categories'] = $this->app['models']['category']->list();

        $data['category'] = $category;

        $filter = [];

        if(isset($category['category_id'])) {
            $filter['product_category_id'] = $category['category_id'];
        } else {
            $category = [
                'category_id' => '0',
                'category_title' => 'Svi proizvodi',
                'picture_thumb_url' => $this->app['config']['website']['url'] . 'img/category.jpg'
            ];
        }

        $data['products'] = $this->app['models']['product']->list($filter);

        $data['has_products'] = count($data['products']) > 0;

        $data['category_id'] = $category['category_id'];
        $data['category_title'] = $category['category_title'];
        $data['category_picture'] = $category['picture_thumb_url'];

        $view = $this->app['view'];
        $view->render('header', $data);
        $view->render('products', $data);
        $view->render('footer', $data);
    }
}