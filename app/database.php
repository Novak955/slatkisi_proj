<?php 
Class Database {
    private $host;
    private $user;
    private $pass;
    private $name;
    private $query;
    protected $isConnected;
    private $mysqli;
    private $charset = 'utf8mb4';
    public function __construct($host, $user, $pass, $name) {
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->name = $name;
        $this->query = "";
        $this->isConnected = false;
        $this->mysqli = false;
        $this->resource = false;
        return $this;
    }

    public function connect() {
        if(!$this->isConnected) {
            $this->mysqli = mysqli_connect($this->host, $this->user, $this->pass, $this->name);
            if(!$this->mysqli) {
                throw new Exception("MySQL Error: \"" . mysqli_connect_error() . "\" while connecting to db. ");
            }
            mysqli_query($this->mysqli, "SET names '" . $this->charset . "'");
            $this->isConnected = true;
        }
        return $this;
    }

    public function query($query) {
        if(!$this->isConnected) {
            throw new Exception('Database Error: Not connected to database!');
        }
        $resource = mysqli_query($this->mysqli, $query);
        if(!$resource) {
            throw new Exception("MySQL Error: \"" . mysqli_error($this->mysqli) . "\" for query \"$query\"");
        }
        return $resource;
    }

    public function fetch($resource) {
        if(!$resource) {
            return [];
        }
        $data = [];
        while($row = mysqli_fetch_assoc($resource)) {
            $data[] = $row;
        }
        return $data;
    }

    public function count($resource) {
        if(!$resource) {
            return 0;
        }
        $count = mysqli_num_rows($resource);
        return $count;
    }

    public function insert_id() {
        return mysqli_insert_id($this->mysqli);
    }

    public function escape($value, $type = 'string') {
        if(!$this->isConnected) {
            throw new Exception('Database Error: Not connected to database!');
        }
        if($type == 'string') {
            return mysqli_real_escape_string($this->mysqli, $value);
        } else {
            return (int) $value;
        }
    }
}