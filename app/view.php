<?php 
Class View {
    private $m;
    public function __construct($mustache) {
        $this->m = $mustache;
    }
    public function render($template, $data) {
        $html_file = __DIR__ . '/views/' . $template . '.html';
        if(!is_file($html_file)) {
            throw new Exception("Couldn't load template \"$template\" from ./app/views/!");
        }
        $html = file_get_contents($html_file);
        if(!is_array($data)) {
            $data = [];
        }
        echo $this->m->render($html, $data);
    }
    public function json($data, $terminate = false) {
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        if($terminate) {
            exit;
        }
    }
}