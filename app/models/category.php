<?php 
Class CategoryModel {
    private $db;
    public function __construct($db) {
        $this->db = $db;
    }
    public function list($showHidden = false) {
        $where = "";
        if(!$showHidden) {
            $where = "WHERE `CA`.`category_is_hidden` <> '1'";
        }
        $query = "SELECT * FROM `category` `CA` LEFT JOIN `picture` `PI` ON `CA`.`category_picture_id` = `PI`.`picture_id` $where ORDER BY `CA`.`category_title` ASC";
        $res = $this->db->query($query);
        $data = $this->db->fetch($res);
        return $data;
    }
    public function getByTitle($title) {
        $title_ = $this->db->escape($title);
        $where = "WHERE `CA`.`category_title` = '$title_'";
        return $this->_getOne($where);
    }
    public function getById($id) {
        $id_ = $this->db->escape($id);
        $where = "WHERE `CA`.`category_id` = '$id_'";
        return $this->_getOne($where);
    }
    public function add($title, $picture_id) {
        $title_ = $this->db->escape($title);
        $picture_id_ = $this->db->escape($picture_id, 'int');
        $query = "INSERT INTO `category` (`category_title`, `category_picture_id`, `category_is_hidden`) VALUES ('$title_', '$picture_id_', '0')";
        $this->db->query($query);
    }
    public function update($id, $title, $picture_id) {
        $id_ = $this->db->escape($id);
        $title_ = $this->db->escape($title);
        $picture_id_ = $this->db->escape($picture_id, 'int');
        $query = "UPDATE `category` SET `category_title` = '$title_', `category_picture_id` = '$picture_id_' WHERE `category_id` = '$id_' LIMIT 1";
        $this->db->query($query);
    }
    public function setVisibility($id, $visibility) {
        $id_ = $this->db->escape($id);
        $visibility_ = $this->db->escape($visibility, 'int');
        $query = "UPDATE `category` SET `category_is_hidden` = '$visibility_' WHERE `category_id` = '$id_' LIMIT 1";
        $this->db->query($query);
    }
    private function _getOne($where) {
        $query = "SELECT * FROM `category` `CA` LEFT JOIN `picture` `PI` ON `CA`.`category_picture_id` = `PI`.`picture_id` $where LIMIT 1";
        $res = $this->db->query($query);
        $data = $this->db->fetch($res);
        $item = false;
        if(isset($data[0]) && count($data[0]) > 0) {
            $item = $data[0];
        }
        return $item;
    }
}