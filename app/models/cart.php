<?php 
Class CartModel {
    private $sp;
    private $db;
    private $product;
    public function __construct($db, $product) {
		$this->product = $product;
		$this->db = $db;
		$sp = $this->app->config['website']['session_prefix'];
        $this->sp = $sp;
    }
    public function total() {
		$total = 0;
		foreach($this->get() as $item) {
			$quantity = $item['quantity'];
			$price = $item['price'];
			$amount = $price * $quantity;
			$total += $amount;
		}
		return $total;
	}
    public function get() {
		if(!isset($_SESSION[$this->sp . 'cart']) || !is_array($_SESSION[$this->sp . 'cart'])) {
			$_SESSION[$this->sp . 'cart'] = [];
		}
		return $_SESSION[$this->sp . 'cart'] ?: [];
    }
	private function _getProduct($id) {
		return $this->product->getById($id);
	}
	public function exists($id) {
		$cart = $this->get();
		foreach($cart as $item) {
			if($item['id'] == $id) {
				return true;
			}
		}
		return false;
	}
	public function update($id, $quantity) {
		$cart = $this->get();
		foreach($cart as $i => $item) {
			if($item['id'] == $id) {
				$item['quantity'] = $quantity;
				$cart[$i] = $item;
				$_SESSION[$this->sp . 'cart'] = $cart;
				return true;
			}
		}
		return false;
	}
	public function remove($id) {
		$cart = $this->get();
		foreach($cart as $i => $item) {
			if($item['id'] == $id) {
				unset($cart[$i]);
				$cart = array_values($cart);
				$_SESSION[$this->sp . 'cart'] = $cart;
				return true;
			}
		}
		return false;
	}
	public function empty() {
		$_SESSION[$this->sp . 'cart'] = [];
	}
	public function add($id, $quantity) {
		$item = $this->_getProduct($id);
		$_SESSION[$this->sp . 'cart'][] = [
			'id' => $id,
			'quantity' => $quantity,
			'title' => $item['product_title'],
			'price' => $item['product_price_per_unit']
		];
	}
}