<?php 
Class ProductModel {
    private $db;
    public function __construct($db) {
        $this->db = $db;
    }
    public function list($filter = []) {
        $where = '';
        if(isset($filter['product_category_id']) && $filter['product_category_id'] && count($filter) == 1) {
            $product_category_id = $this->db->escape($filter['product_category_id']);
            $where = " WHERE `PR`.`product_category_id` = '$product_category_id' ";
        }
        $query = "SELECT * FROM `product` `PR` LEFT JOIN `picture` `PI` ON `PR`.`product_picture_id` = `PI`.`picture_id` $where ORDER BY `PR`.`product_title` ASC";
        $res = $this->db->query($query);
        $data = $this->db->fetch($res);
        return $data;
    }
    public function getByTitle($title) {
        $title_ = $this->db->escape($title);
        $where = "WHERE `PR`.`product_title` = '$title_'";
        return $this->_getOne($where);
    }
    public function getById($id) {
        $id_ = $this->db->escape($id);
        $where = "WHERE `PR`.`product_id` = '$id_'";
        return $this->_getOne($where);
    }
    public function add($title, $short_description, $long_description, $picture_id, $unit_of_measure, $price_per_unit, $category_id) {
        $title_ = $this->db->escape($title);
        $short_description_ = $this->db->escape($short_description);
        $long_description_ = $this->db->escape($long_description);
        $picture_id_ = $this->db->escape($picture_id, 'int');
        $unit_of_measure_ = $this->db->escape($unit_of_measure);
        $price_per_unit_ = $this->db->escape($price_per_unit);
        $category_id_ = $this->db->escape($category_id);
        $query = "INSERT INTO `product` (`product_title`, `product_short_description`, `product_long_description`, `product_picture_id`, `product_unit_of_measure`, `product_price_per_unit`, `product_category_id`) VALUES ('$title_', '$short_description_', '$long_description_', '$picture_id_', '$unit_of_measure_', '$price_per_unit_', '$category_id_')";
        $this->db->query($query);
    }
    public function update($id, $title, $short_description, $long_description, $picture_id, $unit_of_measure, $price_per_unit, $category_id) {
        $id_ = $this->db->escape($id);
        $title_ = $this->db->escape($title);
        $short_description_ = $this->db->escape($short_description);
        $long_description_ = $this->db->escape($long_description);
        $picture_id_ = $this->db->escape($picture_id, 'int');
        $unit_of_measure_ = $this->db->escape($unit_of_measure);
        $price_per_unit_ = $this->db->escape($price_per_unit);
        $category_id_ = $this->db->escape($category_id);
        $query = "UPDATE `product` SET `product_title` = '$title_', `product_short_description` = '$short_description_', `product_long_description` = '$long_description_', `product_picture_id` = '$picture_id_', `product_unit_of_measure` = '$unit_of_measure_', `product_price_per_unit` = '$price_per_unit_', `product_category_id` = '$category_id_' WHERE `product_id` = '$id_' LIMIT 1";
        $this->db->query($query);
    }
    private function _getOne($where) {
        $query = "SELECT * FROM `product` `PR` LEFT JOIN `picture` `PI` ON `PR`.`product_picture_id` = `PI`.`picture_id` $where LIMIT 1";
        $res = $this->db->query($query);
        $data = $this->db->fetch($res);
        $item = false;
        if(isset($data[0]) && count($data[0]) > 0) {
            $item = $data[0];
        }
        return $item;
    }
}