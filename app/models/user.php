<?php 
Class UserModel {
    private $db;
    public function __construct($db) {
        $this->db = $db;
    }
    public function getById($id = '') {
        $id_ = $this->db->escape($id);
        $where = "WHERE `user_id` = '$id_'";
        return $this->_getOne($where);
    }
    private function _getOne($where) {
        $query = "SELECT * FROM `user` $where LIMIT 1";
        $res = $this->db->query($query);
        $data = $this->db->fetch($res);
        $item = false;
        if(isset($data[0]) && count($data[0]) > 0) {
            $item = $data[0];
        }
        return $item;
    }
    public function getByCredentials($username = '', $password = '') {
        $username_ = $this->db->escape($username);
        $password_ = $this->db->escape($password);
        $where = "WHERE `username` = '$username_' AND `password` = '$password_'";
        return $this->_getOne($where);
    }
}