<?php 
Class PictureModel {
    private $db;
    public function __construct($db) {
        $this->db = $db;
    }
    public function list() {
        $query = "SELECT * FROM `picture`";
        $res = $this->db->query($query);
        $data = $this->db->fetch($res);
        return $data;
    }
    public function getById($id) {
        $id_ = $this->db->escape($id, 'int');
        $where = "WHERE `picture_id` = '$id_'";
        return $this->_getOne($where);
    }
    public function add($filename, $url, $thumb_url) {
        $filename_ = $this->db->escape($filename);
        $url_ = $this->db->escape($url);
        $thumb_url_ = $this->db->escape($thumb_url);
        $query = "INSERT INTO `picture` (`picture_filename`, `picture_url`, `picture_thumb_url`) VALUES ('$filename_', '$url_', '$thumb_url_')";
        $this->db->query($query);
        return $this->db->insert_id();
    }
    private function _getOne($where) {
        $query = "SELECT * FROM `picture` $where LIMIT 1";
        $res = $this->db->query($query);
        $data = $this->db->fetch($res);
        $item = false;
        if(isset($data[0]) && count($data[0]) > 0) {
            $item = $data[0];
        }
        return $item;
    }
}