<?php 
Class Login {
    protected $redirect_uri;
    private $session_prefix;
    public function __construct($session_prefix = 'myapp_') {
        $this->session_prefix = $session_prefix;
    }
    public function isLoggedIn() {
        return (isset($_SESSION[$this->session_prefix . 'adminLogin']) && !empty($_SESSION[$this->session_prefix . 'adminLogin']));
    }
    public function login($username) {
        $_SESSION[$this->session_prefix . 'adminLogin'] = $username;
    }
    public function logout() {
        $_SESSION[$this->session_prefix . 'adminLogin'] = null;
        unset($_SESSION[$this->session_prefix . 'adminLogin']);
    }
    public function setRedirectUri($redirect_uri) {
        $this->redirect_uri = $redirect_uri;
    }
    public function restrict() {
        if(!$this->isLoggedIn()) {
            Flight::redirect($this->redirect_uri);
            Flight::halt();
            exit;
        }
    }
}