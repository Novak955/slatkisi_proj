/*function categories(isResizeEvent) {
    var categories_count = $('.group .categories .category').length + 1
    var category_width = 170;
    var categories_width = categories_count * category_width;
    $('.group .categories').css({
        width: categories_width + 'px'
    })
}

$(function() {
    categories();
    $(window).resize(function() {
        categories(true);
    })
})*/

$(function() {
    $('.group .categories .category').on('click', function () {
        window.location.href = $(this).find('.category-title a').attr('href')
    })
    $('.categories-button').click(function() {
        $('.categories-menu').slideToggle()
    })
    $('.products .product').on('click', function () {
        window.location.href = $(this).find('.product-title a').attr('href')
    })
})