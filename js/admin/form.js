$(function () {
    $('select[data-autovalue-select]').each(function() {
        if($(this).attr('data-value')) {
            var that = this
            $(this).find('option[selected]').removeAttr('selected')
            $(this).find('option').each(function() {
                if($(this).attr('value') == $(that).attr('data-value')) {
                    $(this).attr('selected', '1')
                }
            })
        }
    })
})