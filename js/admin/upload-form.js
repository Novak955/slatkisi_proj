function fileUploadFormHandler() {
    var form = $('form[data-fileupload-form]')
    var action = form.attr('data-fileupload-action')
    var file_field = $('input[type="file"][data-fileupload-field]')
    var value_field = $('input[type="hidden"][data-fileupload-value]')
    if(form.attr('data-not-required-fileupload') == '1') {
        if(!file_field[0].files[0]) {
            return genericFormCallback_NoFileUpload(form)
        } else {
            if(!confirm('Da li ste sigurni da želite da promenite sliku?')) {
                return genericFormCallback_NoFileUpload(form)
            }
        }
    }
    var formData = new FormData()
    formData.append('file', file_field[0].files[0])
    $.ajax({
        url : action,
        type : 'POST',
        data : formData,
        processData: false,
        contentType: false,
        success : function(data) {
            if(data.success) {
                value_field.val(data.response_id)
                return genericFormCallback_NoFileUpload(form)
            } else {
                if(!data.error) { data.error = 'Došlo je do greške!' }
                alert(data.error)
            }
        }
    })
}

function genericFormCallback_NoFileUpload(form) {
    var action = form.attr('action')
    var data = {}
    form.find('input, textarea, select').each(function() {
        if($(this).attr('type') != 'file') {
            data[$(this).attr('name')] = $(this).val()
        }
    })
    $.ajax({
        url : action,
        type : 'POST',
        data : data,
        success : function(data) {
            if(data.success) {
                if(data.redirect) {
                    window.location.href = data.redirect
                } else {
                    window.location.href = window.site_url
                }
            } else {
                if(!data.error) { data.error = 'Došlo je do greške!' }
                alert(data.error)
            }
        }
    })
}