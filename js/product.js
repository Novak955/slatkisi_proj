$(function() {
    $('.order-qty .order-qty-plus').on('click', function () {
        var quantity = parseInt($('#theProductOrderQuantity').text())
        quantity += 1; $('#theProductOrderButton').removeClass('disabled') 
        $('#theProductOrderQuantity').text(quantity)
        var price = parseFloat($('#productPricePerUnit').text().replace(/\s+?din/g, ''))
        var total = quantity * price
        $('#theProductOrderTotal').text(total)
    })
    $('.order-qty .order-qty-minus').on('click', function () {
        var quantity = parseInt($('#theProductOrderQuantity').text())
        quantity -= 1
        if(quantity < 1) { quantity = 0; $('#theProductOrderButton').addClass('disabled') }
        $('#theProductOrderQuantity').text(quantity)
        var price = parseFloat($('#productPricePerUnit').text().replace(/\s+?din/g, ''))
        var total = quantity * price
        $('#theProductOrderTotal').text(total)
    })
	$('#theProductOrderButton').on('click', function() {
		var id = $(this).attr('data-id')
		var price = parseFloat($('#productPricePerUnit').text().replace(/\s+?din/g, ''))
		var qty = parseInt($('#theProductOrderQuantity').text())
		var title = $('#theProductTitle').text()
		cart_add(id, qty, function (success) {
			if(success) {
				cart_update()
			}
		})
	})
})