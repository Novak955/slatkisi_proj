$(function() {
	$('#theMyCart').on('click', function () {
		cartui_display()
	})
	$(document).on('mousedown', function (e) {
		if(!$('.modal').is(e.target) && $('.modal').has(e.target) && $('.modal').has(e.target).length == 0) {
			cartui_hide()
		}
	})
	$('#cartItems').on('change', 'input.quantity', function () {
		var id = $(this).attr('data-id')
		var val = $(this).val()
		id = parseInt(id); val = parseInt(val)
		cart_update_qty(id, val, function (status) {
			cartui_update()
		})
	})
})

function cartui_display() {
	$('#theCartModal').fadeIn(300)
	cartui_update()
}

function cartui_hide() {
	$('#theCartModal').fadeOut(300)
}

function cartui_update() {
	cart_total(function (success, total) {
		$('#cartTotal').text(total)
	})
	$('.cart-total').show()
	$('.cart-more').show()
	cart_list(function (success, items) {
		var items_html = '<div class="items">'
		$.each(items, function (i, item) {
			var total = item.price * item.quantity
			items_html += '<div class="item"><h3>' + item.title + '</h3><p class="pricing">Cena: <span class="the-price-item">' + item.price + ' din</span></p><div class="right-s"><p class="times"> &times; </p><input id="quantity' + item.id + '" value="' + item.quantity + '" class="quantity" data-id="' + item.id + '" type="number" min="0" max="999" /><p class="total"> = <span class="the-total-item">' + total + ' din</span></p><div class="clearfix"></div></div><div class="clearfix"></div></div>'
		})
		if(items.length < 1) {
			items_html += '<div class="no-items"><p>Nemate proizvoda u korpi. </p></div>'
			$('.cart-total').hide()
			$('.cart-more').hide()
		}
		items_html += '</div>'
		$('#cartItems').html(items_html)
	})
}