function cart_total(cb) {
	$.post(window.site_url + 'api/cart', { action: 'total' }, function (data) {
		if(cb) { cb((data && data.success) ? true : false, data.total || 0) }
	})
}

function cart_list(cb) {
	$.post(window.site_url + 'api/cart', { action: 'list' }, function (data) {
		if(cb) { cb((data && data.success) ? true : false, data.items || []) }
	})
}

function cart_update() {
	cart_list(function (success, items) {
		if(success) {
			console.log('Cart items: ', items)
		}
	})
	cart_total(function (success, total) {
		if(success) {
			console.log('Cart total: ', total)
		}
	})
}

function cart_add(item_id, qty, cb) {
	$.post(window.site_url + 'api/cart', { action: 'check', product: item_id }, function (data) {
		if(data && data.success && data.result) {
			cart_update_qty(item_id, qty, cb) // TODO: Proveriti radnu logiku
		} else {
			$.post(window.site_url + 'api/cart', { action: 'add', id: item_id, quantity: qty }, function (data) {
				if(cb) { cb((data && data.success) ? true : false) }
			})
		}
	})
}

function cart_remove(item_id, cb) {
	$.post(window.site_url + 'api/cart', { action: 'remove', product: item_id }, function (data) {
		if(cb) { cb((data && data.success) ? true : false) }
	})
}

function cart_update_qty(item_id, qty, cb) {
	$.post(window.site_url + 'api/cart', { action: 'update', product: item_id, quantity: qty }, function (data) {
		if(cb) { cb((data && data.success) ? true : false) }
	})
}

function cart_empty(cb) {
	$.post(window.site_url + 'api/cart', { action: 'empty' }, function (data) {
		if(cb) { cb((data && data.success) ? true : false) }
	})
}