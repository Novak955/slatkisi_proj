<?php 
session_start(); 

require 'app/vendor/autoload.php';
require 'app/configuration/database.php';
require 'app/configuration/website.php';
require 'app/models/product.php';
require 'app/models/category.php';
require 'app/models/picture.php';
require 'app/models/user.php';
require 'app/models/cart.php';
require 'app/controllers/home.php';
require 'app/controllers/products.php';
require 'app/controllers/product.php';
require 'app/controllers/checkout.php';
require 'app/controllers/api/cart.php';
require 'app/controllers/admin/home.php';
require 'app/controllers/admin/login.php';
require 'app/controllers/admin/categories.php';
require 'app/controllers/admin/addcategory.php';
require 'app/controllers/admin/products.php';
require 'app/controllers/admin/addproduct.php';
require 'app/controllers/admin/editproduct.php';
require 'app/controllers/admin/editcategory.php';
require 'app/controllers/admin/api/pictureupload.php';
require 'app/utilities.php';
require 'app/database.php';
require 'app/login.php';
require 'app/view.php';

$db = new Database($config['database']['host'], $config['database']['user'], $config['database']['pass'], $config['database']['name']);
$db->connect();
$m = new Mustache_Engine();
$view = new View($m);
$product = new ProductModel($db);
$category = new CategoryModel($db);
$cart = new CartModel($db, $product);
$login = new Login($config['website']['session_prefix']);
$user = new UserModel($db);
$picture = new PictureModel($db);

$login->setRedirectUri($config['website']['url'] . 'admin/login');

$app = [];
$app['config'] = $config;
$app['view'] = $view;
$app['db'] = $db;
$app['user'] = $user;
$app['login'] = $login;
$app['models'] = [];
$app['models']['product'] = $product;
$app['models']['category'] = $category;
$app['models']['picture'] = $picture;
$app['models']['cart'] = $cart;
$app['controllers'] = [];
$app['controllers']['admin'] = [];
$app['controllers']['home'] = new HomeController($app);
$app['controllers']['products'] = new ProductsController($app);
$app['controllers']['product'] = new ProductController($app);
$app['controllers']['checkout'] = new CheckoutController($app);
$app['controllers']['api'] = [];
$app['controllers']['api']['cart'] = new CartAPIController($app);
$app['controllers']['admin']['home'] = new HomeAdminController($app);
$app['controllers']['admin']['login'] = new LoginAdminController($app);
$app['controllers']['admin']['categories'] = new CategoriesAdminController($app);
$app['controllers']['admin']['addcategory'] = new AddCategoryAdminController($app);
$app['controllers']['admin']['products'] = new ProductsAdminController($app);
$app['controllers']['admin']['addproduct'] = new AddProductAdminController($app);
$app['controllers']['admin']['editproduct'] = new EditProductAdminController($app);
$app['controllers']['admin']['editcategory'] = new EditCategoryAdminController($app);
$app['controllers']['admin']['api'] = [];
$app['controllers']['admin']['api']['pictureupload'] = new PictureUploadAPIAdminController($app);

Flight::route('/', function() use ($app) {
    return $app['controllers']['home']->run([]);
});

Flight::route('/proizvodi', function() use ($app) {
    return $app['controllers']['products']->run([]);
});

Flight::route('/checkout', function() use ($app) {
    return $app['controllers']['checkout']->run([]);
});

Flight::route('/proizvod/@id', function($id) use ($app) {
    return $app['controllers']['product']->run([ 'id' => $id ]);
});

Flight::route('/api/cart', function() use ($app) {
    return $app['controllers']['api']['cart']->run([]);
});

Flight::route('/admin', function() use ($app) {
    $app['login']->restrict();
    return $app['controllers']['admin']['home']->run([]);
});

Flight::route('/admin/login', function() use ($app) {
    return $app['controllers']['admin']['login']->run([]);
});

Flight::route('/admin/categories', function() use ($app) {
    return $app['controllers']['admin']['categories']->run([]);
});

Flight::route('/admin/categories/add', function() use ($app) {
    return $app['controllers']['admin']['addcategory']->run([]);
});

Flight::route('/admin/categories/@id/edit', function($id) use ($app) {
    return $app['controllers']['admin']['editcategory']->run([ 'id' => $id ]);
});

Flight::route('/admin/categories/@id/hide', function($id) use ($app) {
    return $app['controllers']['admin']['editcategory']->setVisibility([ 'id' => $id, 'hide' => '1' ]);
});

Flight::route('/admin/categories/@id/show', function($id) use ($app) {
    return $app['controllers']['admin']['editcategory']->setVisibility([ 'id' => $id, 'hide' => '0' ]);
});

Flight::route('/admin/products/add', function() use ($app) {
    return $app['controllers']['admin']['addproduct']->run([]);
});

Flight::route('/admin/products', function() use ($app) {
    return $app['controllers']['admin']['products']->run([]);
});

Flight::route('/admin/products/@id/edit', function($id) use ($app) {
    return $app['controllers']['admin']['editproduct']->run([ 'id' => $id ]);
});

Flight::route('/admin/api/picture-upload', function() use ($app) {
    return $app['controllers']['admin']['api']['pictureupload']->run([]);
});

Flight::start();