-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

-- IME BAZE JE slatkisi_proj

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_title` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_picture_id` int(11) NOT NULL,
  `category_is_hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_title` (`category_title`(190)),
  KEY `category_picture_id` (`category_picture_id`),
  CONSTRAINT `fk_category_picture_id` FOREIGN KEY (`category_picture_id`) REFERENCES `picture` (`picture_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `picture`;
CREATE TABLE `picture` (
  `picture_id` int(11) NOT NULL AUTO_INCREMENT,
  `picture_filename` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture_thumb_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`picture_id`),
  UNIQUE KEY `picture_id` (`picture_id`),
  UNIQUE KEY `picture_url` (`picture_filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_title` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_long_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_picture_id` int(11) NOT NULL,
  `product_unit_of_measure` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price_per_unit` decimal(10,2) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `product_category_id` (`product_category_id`),
  KEY `product_unit_of_measure` (`product_unit_of_measure`),
  KEY `product_price_per_unit` (`product_price_per_unit`),
  KEY `product_title` (`product_title`(190)),
  KEY `product_picture_id` (`product_picture_id`),
  CONSTRAINT `fk_product_picture_id` FOREIGN KEY (`product_picture_id`) REFERENCES `picture` (`picture_id`),
  CONSTRAINT `fk_product_category_id` FOREIGN KEY (`product_category_id`) REFERENCES `category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uq_username` (`username`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `user` (`user_id`, `username`, `password`) VALUES
(1,	'admin',	'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec');

-- 2018-11-06 20:16:51
